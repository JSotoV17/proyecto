<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS, HEAD");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header("Content-Type: Application/JSON; charset=UTF-8");

include "./config.php";

$postjson = json_decode(file_get_contents('php://input'), true);
$today = date('Y-m-d');

if ($postjson['aksi'] == "register") {

    $password = md5($postjson['password']);
    $query = mysqli_query($mysqli, "INSERT INTO bcard_user(username, password, email, status, created_at, arrendatario) VALUES"
            . " ('$postjson[username]' ,'$password', '$postjson[email]', 'y', '$today', '$postjson[arrendatario]')");

    if ($query) {
        $result = json_encode(array('success' => true));
    } else {
        $result = json_encode(array('success' => false, 'msg' => 'Error, Intente de nuevo'));
    }

    echo $result;
} 


elseif ($postjson['aksi'] == "addprop") {

    $query = mysqli_query($mysqli, "INSERT INTO property(user_id, provincia, direccion, latitud, longitud, cuartos, capacidad, costodiario,telpropietario) VALUES"
            . " ('$postjson[user_id]' ,'$postjson[provincia]', '$postjson[direccion]', '$postjson[latitud]', '$postjson[longitud]', "
            . " '$postjson[cuartos]','$postjson[capacidad]','$postjson[costodiario]','$postjson[telpropietario]')");

    if ($query) {
        $result = json_encode(array('success' => true));
    } else {
        $result = json_encode(array('success' => false, 'msg' => 'Error, Intente de nuevo'));
    }

    echo $result;
}

elseif ($postjson['aksi'] == "getpropertyperson") {
    $data = array();
    $query = mysqli_query($mysqli, "SELECT * FROM property WHERE user_id='$postjson[user_id]'");

    while ($row = mysqli_fetch_array($query)) {
         $data[] = array(
            'id' => $row['id'],
            'user_id' => $row['user_id'],
            'provincia' => $row['provincia'],
            'direccion' => $row['direccion'],
            'latitud' => $row['latitud'],
            'longitud' => $row['longitud'],
            'cuartos' => $row['cuartos'],
            'capacidad' => $row['capacidad'],
            'costodiario' => $row['costodiario'],
            'telpropietario' => $row['telpropietario']
        );
    }
    if ($query)
        $result = json_encode(array('success' => true, 'result' => $data));
    else
        $result = json_encode(array('success' => false));
    echo $result;
}

elseif ($postjson['aksi'] == "delproperty") {
    $query = mysqli_query($mysqli, "DELETE FROM property WHERE id='$postjson[id]'");

    if ($query)
        $result = json_encode(array('success' => true, 'result' => 'success'));
    else
        $result = json_encode(array('success' => false, 'result' => 'error'));
    echo $result;
}


elseif ($postjson['aksi'] == 'getpropertyID') {
    $data = array();
    $query = mysqli_query($mysqli, "SELECT * FROM property WHERE id='$postjson[id]'");
    while ($row = mysqli_fetch_array($query)) {
        $data[] = array(
            'id' => $row['id'],
            'user_id' => $row['user_id'],
            'provincia' => $row['provincia'],
            'direccion' => $row['direccion'],
            'latitud' => $row['latitud'],
            'longitud' => $row['longitud'],
            'cuartos' => $row['cuartos'],
            'capacidad' => $row['capacidad'],
            'costodiario' => $row['costodiario'],
            'telpropietario' => $row['telpropietario']
        );
    }
    if ($query)
        $result = json_encode(array('success' => true, 'result' => $data));
    else
        $result = json_encode(array('success' => false));
    echo $result;
}


elseif ($postjson['aksi'] == 'upproperty') {
    $query = mysqli_query($mysqli, "UPDATE property SET
  provincia='$postjson[provincia]',
      direccion='$postjson[direccion]',
          latitud='$postjson[latitud]',
              longitud='$postjson[longitud]',
              cuartos='$postjson[cuartos]',
                  capacidad='$postjson[capacidad]',
                      costodiario='$postjson[costodiario]',
                          telpropietario='$postjson[telpropietario]' "
            . "WHERE id='$postjson[id]'");

    if ($query)
        $result = json_encode(array('success' => true, 'result' => 'success'));
    else
        $result = json_encode(array('success' => false, 'result' => 'error'));
    echo $result;
}


elseif ($postjson['aksi'] == "login") {
    $data = array();
    $password = md5($postjson['password']);
    $query = mysqli_query($mysqli, "SELECT * FROM bcard_user WHERE email='$postjson[email]' AND password='$password'");
    $check = mysqli_num_rows($query);

    if ($check > 0) {
        $data = mysqli_fetch_array($query);
        $data[] = array(
            'user_id' => $data['user_id'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => $data['password'],
            'provincia' => $data['provincia'],
            'pais' => $data['pais'],
            'arrendatario' => $data['arrendatario'],
        );

        if ($data['status'] == 'y') {
            $result = json_encode(array('success' => true, 'result' => $data));
        } else {
            $result = json_encode(array('success' => false, 'msg' => 'Cuenta Inactiva. Por favor comuniquese con nosotros!'));
        }
    } else {
        $result = json_encode(array('success' => false, 'msg' => 'Cuenta no Registrada'));
    }
    echo $result;
}

elseif ($postjson['aksi'] == 'upuseract') {
    $query = mysqli_query($mysqli, "UPDATE user_act SET
        id='$postjson[id]', 
         user_id='$postjson[user_id]',    
         username='$postjson[username]' "
            . "WHERE id='$postjson[id]'");

    if ($query)
        $result = json_encode(array('success' => true, 'result' => 'success'));
    else
        $result = json_encode(array('success' => false, 'result' => 'error'));
    echo $result;
}

elseif ($postjson['aksi'] == "listuseract") {
    $data = array();
    $query = mysqli_query($mysqli, "SELECT * FROM user_act WHERE id='1'");

    while ($row = mysqli_fetch_array($query)) {
        $data[] = array(
            'id' => $row['id'],
            'user_id' => $row['user_id'],
            'username' => $row['username'],        
        );
    }
    if ($query)
        $result = json_encode(array('success' => true, 'result' => $data));
    else
        $result = json_encode(array('success' => false));
    echo $result;
}

elseif ($postjson['aksi'] == "listproperty") {
    $data = array();
    $query = mysqli_query($mysqli, "SELECT * FROM property");

  while ($row = mysqli_fetch_array($query)) {
         $data[] = array(
            'id' => $row['id'],
            'user_id' => $row['user_id'],
            'provincia' => $row['provincia'],
            'direccion' => $row['direccion'],
            'latitud' => $row['latitud'],
            'longitud' => $row['longitud'],
            'cuartos' => $row['cuartos'],
            'capacidad' => $row['capacidad'],
            'costodiario' => $row['costodiario'],
            'telpropietario' => $row['telpropietario']
        );
    }
    if ($query)
        $result = json_encode(array('success' => true, 'result' => $data));
    else
        $result = json_encode(array('success' => false));
    echo $result;
}


elseif ($postjson['aksi'] == "adreserv") {

    $query = mysqli_query($mysqli, "INSERT INTO reservation(user_id, username, id_property, provincia, direccion, telefono,entrada,salida) VALUES"
            . " ('$postjson[user_id]' ,'$postjson[username]', '$postjson[id_property]', "
            . " '$postjson[provincia]','$postjson[direccion]','$postjson[telefono]','$postjson[entrada]', '$postjson[salida]' )");

    if ($query) {
        $result = json_encode(array('success' => true));
    } else {
        $result = json_encode(array('success' => false, 'msg' => 'Error, Intente de nuevo'));
    }

    echo $result;
}
?>