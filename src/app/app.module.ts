import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { HttpClientModule, HttpClient } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

import { Facebook } from "@ionic-native/facebook/ngx";

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";

// Modal Pages
import { ImagePageModule } from "./pages/modal/image/image.module";
import { SearchFilterPageModule } from "./pages/modal/search-filter/search-filter.module";

// Components

import { Geolocation } from "@ionic-native/geolocation/ngx";

import { HttpModule } from "@angular/http";
import { PostProvider } from "../providers/post-provider";

import { IonicStorageModule } from "@ionic/storage";

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    ImagePageModule,
    SearchFilterPageModule,
    HttpModule,
    IonicStorageModule.forRoot({
      name: "exbd",
    }),
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    HttpClient,
    PostProvider,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Facebook,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
