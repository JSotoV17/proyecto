import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'register', loadChildren: './pages/register/register.module#RegisterPageModule' },
  { path: 'about', loadChildren: './pages/about/about.module#AboutPageModule' },
  { path: 'edit-profile', loadChildren: './pages/edit-profile/edit-profile.module#EditProfilePageModule' },
  { path: 'home-results', loadChildren: './pages/home-results/home-results.module#HomeResultsPageModule' },
  { path: 'contact-us', loadChildren: './pages/contact-us/contact-us.module#ContactUsPageModule' },
  { path: 'loginemail', loadChildren: './pages/loginemail/loginemail.module#LoginemailPageModule' },
  { path: 'home-tenants', loadChildren: './pages/home-tenants/home-tenants.module#HomeTenantsPageModule' },
  { path: 'add-property', loadChildren: './pages/home-tenants/add-property/add-property.module#AddPropertyPageModule' },
  { path: 'edit-property/:id', loadChildren: './pages/home-tenants/edit-property/edit-property.module#EditPropertyPageModule' },
  { path: 'list-property', loadChildren: './pages/home-tenants/list-property/list-property.module#ListPropertyPageModule' },
  { path: 'view-property/:id', loadChildren: './pages/home-tenants/view-property/view-property.module#ViewPropertyPageModule' },
  { path: 'list', loadChildren: './pages/home-results/list/list.module#ListPageModule' },
  { path: 'view/:id', loadChildren: './pages/home-results/view/view.module#ViewPageModule' },
  { path: 'reservation/:id', loadChildren: './pages/home-results/reservation/reservation.module#ReservationPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {}