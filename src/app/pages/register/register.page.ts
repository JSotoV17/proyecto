import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  NavController,
  MenuController,
  LoadingController,
  ToastController,
} from "@ionic/angular";
import { PostProvider } from "../../../providers/post-provider";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"],
})
export class RegisterPage implements OnInit {
  public onRegisterForm: FormGroup;

  fullname: string = "";
  email: string = "";
  password: string = "";
  arrendatario: number = 0;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private postPvdr: PostProvider,
    public toastctrl: ToastController
  ) {}

  ngOnInit() {
    this.onRegisterForm = this.formBuilder.group({
      fullName: [null, Validators.compose([Validators.required])],
      email: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
      arrendatario: 0,
    });
  }

  async prosesRegister() {
    let body = {
      username: this.fullname,
      password: this.password,
      email: this.email,
      arrendatario: this.arrendatario,
      aksi: "register",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        const toast = await this.toastctrl.create({
          message: "Bienvenida(o). Registro Correcto!",
          duration: 3000,
          cssClass: "toast-message",
        });
        toast.present();

        const loader = await this.loadingCtrl.create({
          duration: 2000,
        });

        loader.present();
        loader.onWillDismiss().then(() => {
          //lo envio a home o lo envio a login
          this.navCtrl.navigateRoot("/");
        });
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });
  }

  goToLogin() {
    this.navCtrl.navigateRoot("/");
  }
}
