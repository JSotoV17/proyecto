import { Component, OnInit } from "@angular/core";
import { NavController, LoadingController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { PostProvider } from "../../../../providers/post-provider";
import { ToastController } from "@ionic/angular";
import { Storage } from "@ionic/storage";

declare var google;

@Component({
  selector: "app-view-property",
  templateUrl: "./view-property.page.html",
  styleUrls: ["./view-property.page.scss"],
})
export class ViewPropertyPage implements OnInit {
  searchTerm: String = "";
  property: any = [];
  argumentos: number;

  constructor(
    public navCtrl: NavController,
    public geolocation: Geolocation,
    public homeService: PostProvider,
    public loadingCtrl: LoadingController,
    public activeRoute: ActivatedRoute,
    private postPvdr: PostProvider,
    public toastctrl: ToastController,
    private router: Router,
    private storage: Storage
  ) {}

  ngOnInit() {

    this.loadMap();

    this.argumentos = parseInt(this.activeRoute.snapshot.paramMap.get("id"));

    let body = {
      id: this.argumentos,
      aksi: "getpropertyID",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        this.property = data.result;
        const loading = await this.loadingCtrl.create({
          message: "Cargando Lista",
          duration: 1000,
        });
        loading.present();
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });
  
  }

  async loadMap() {

    const loading = await this.loadingCtrl.create({
      message: "Cargando Información",
    });
    loading.present();

    const ruta = await this.geolocation.getCurrentPosition();
    const mLatLng = {
      lat: ruta.coords.latitude,
      lng: ruta.coords.longitude,
    };

    const mapEle: HTMLElement = document.getElementById('map');

    const map = new google.maps.Map(mapEle, { center: mLatLng, zoom: 15 });

    google.maps.event.addListenerOnce(map, 'idle', () => {
      loading.dismiss();
      const marker = new google.maps.Marker({
        position: {
          lat: ruta.coords.latitude,
          lng: ruta.coords.longitude,
        },
        map: map,
        title: ""
      })
    });
}
}
