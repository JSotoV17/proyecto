import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators,FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { PostProvider } from "../../../../providers/post-provider";
import { LoadingController, ToastController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-edit-property",
  templateUrl: "./edit-property.page.html",
  styleUrls: ["./edit-property.page.scss"],
})
export class EditPropertyPage implements OnInit {
  formProductEdit: FormGroup;
  property: any = [];
  argumentos : number;
  
  user_id: string = "";
  provincia: string = "";
  direccion: string = "";
  latitud: number;
  longitud:number;
  cuartos: number;
  capacidad: number;
  costodiario: number;
  telpropietario: string = "";

  constructor(
    private postPvdr: PostProvider,
    public loadingCtrl: LoadingController,
    public toastctrl: ToastController,
    private router: Router,
    public activeRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    
  ) {}

  ngOnInit() {

    this.argumentos = parseInt(this.activeRoute.snapshot.paramMap.get("id"));

    let body = {
      id: this.argumentos,
      aksi: "getpropertyID",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {   
        this.property=data.result;

        this.provincia=this.property[0].provincia;
        this.direccion=this.property[0].direccion;
        this.latitud=parseInt(this.property[0].latitud);
        this.longitud=parseInt(this.property[0].longitud);
        this.cuartos=parseInt(this.property[0].cuartos);
        this.capacidad=parseInt(this.property[0].capacidad);
        this.costodiario=parseInt(this.property[0].costodiario);
        this.telpropietario=this.property[0].telpropietario;

       
        const loading = await this.loadingCtrl.create({
          message: "Cargando Lista",
          duration: 1000,
        });
        loading.present();

      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });

    //validations inputs


    this.formProductEdit = this.formBuilder.group({
      provincia: [null, Validators.compose([Validators.required])],
      direccion: [null, Validators.compose([Validators.required])],
      latitud: [null, Validators.compose([Validators.required])],
      longitud: [null, Validators.compose([Validators.required])],
      cuartos: [null, Validators.compose([Validators.required])],
      capacidad: [null, Validators.compose([Validators.required])],
      costodiario: [null, Validators.compose([Validators.required])],
      telpropietario: [null, Validators.compose([Validators.required])],
    });


  }

  async actualizarPropiedad() {
    let body = {
      id: this.argumentos,
      user_id: this.user_id,
      provincia: this.provincia,
      direccion: this.direccion,
      latitud: this.latitud,
      longitud: this.longitud,
      cuartos: this.cuartos,
      capacidad: this.capacidad,
      costodiario: this.costodiario,
      telpropietario: this.telpropietario,
      aksi: "upproperty",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        const toast = await this.toastctrl.create({
          message: "Propiedad Actualizada",
          duration: 1000,
          cssClass: "toast-message",
        });
        toast.present();

        const loader = await this.loadingCtrl.create({
          duration: 1000,
        });

        loader.present();
        loader.onWillDismiss().then(() => {
          //lo envio a home o lo envio a login
          this.router.navigate(["/home-tenants"]);
        });
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 1000,
          cssClass: "toast-message",
        });
        toast.present();
      }
    });
  }
}
