import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { PostProvider } from "../../../../providers/post-provider";
import { LoadingController, ToastController } from "@ionic/angular";

@Component({
  selector: "app-add-property",
  templateUrl: "./add-property.page.html",
  styleUrls: ["./add-property.page.scss"],
})
export class AddPropertyPage implements OnInit {
  formAdd: FormGroup;
  user_id: string = "";
  provincia: string = "";
  direccion: string = "";
  latitud: number;
  longitud: number;
  cuartos: number;
  capacidad: number;
  costodiario: number;
  telpropietario: string = "";
  user: any = [];

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    public toastctrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
  
    let body = {
      aksi: "listuseract",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      if (data.success) {
        this.user = data.result;
        this.user_id = this.user[0].user_id;
        console.log("ok");
      } else {
        console.log("no");
      }
    });

    //validations inputs

    this.formAdd = this.formBuilder.group({
      provincia: [null, Validators.compose([Validators.required])],
      direccion: [null, Validators.compose([Validators.required])],
      latitud: [null, Validators.compose([Validators.required])],
      longitud: [null, Validators.compose([Validators.required])],
      cuartos: [null, Validators.compose([Validators.required])],
      capacidad: [null, Validators.compose([Validators.required])],
      costodiario: [null, Validators.compose([Validators.required])],
      telpropietario: [null, Validators.compose([Validators.required])],
    });
  }

  async addPropiedad() {
    let body = {
      user_id: this.user_id,
      provincia: this.provincia,
      direccion: this.direccion,
      latitud: this.latitud,
      longitud: this.longitud,
      cuartos: this.cuartos,
      capacidad: this.capacidad,
      costodiario: this.costodiario,
      telpropietario: this.telpropietario,
      aksi: "addprop",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        const toast = await this.toastctrl.create({
          message: "Propiedad Registrada",
          duration: 2000,
          cssClass: "toast-message",
        });
        toast.present();

        const loader = await this.loadingCtrl.create({
          duration: 2000,
        });

        loader.present();
        loader.onWillDismiss().then(() => {
          //lo envio a home o lo envio a login
          this.router.navigate(["/home-tenants"]);
        });
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });
  }
}
