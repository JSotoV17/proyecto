import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPropertyPage } from './list-property.page';

describe('ListPropertyPage', () => {
  let component: ListPropertyPage;
  let fixture: ComponentFixture<ListPropertyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPropertyPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPropertyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
