import { Component, OnInit } from "@angular/core";
import { PostProvider } from "../../../../providers/post-provider";
import { NavController, LoadingController } from "@ionic/angular";
import { ToastController } from "@ionic/angular";
import { Router } from "@angular/router";

@Component({
  selector: "app-list-property",
  templateUrl: "./list-property.page.html",
  styleUrls: ["./list-property.page.scss"],
})
export class ListPropertyPage implements OnInit {
  searchTerm: string = "";
  property: any = [];
  filtro:any=[];
  idPropiedad: number;
  user_id: string = "";
  user: any = [];

  constructor(
    public navCtrl: NavController,
    private postPvdr: PostProvider,
    public loadingCtrl: LoadingController,
    public toastctrl: ToastController,
    private router: Router
  ) {}

  ngOnInit() {
    let body = {
      aksi: "listuseract",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      if (data.success) {
        this.user = data.result;
        this.user_id = this.user[0].user_id;
        this.cargainicial();
        console.log("ok");
      } else {
        console.log("no");
      }
    });
  }

  async cargainicial() {
    let body = {
      user_id: this.user_id,
      aksi: "getpropertyperson",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        this.property = data.result;
        this.showloader();
        this.filtro = this.property;
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });
  }

  setFilteredItems() {
    this.filtro = this.postPvdr.filterItems(this.searchTerm, this.property);
  }

  async showloader() {
    const loading = await this.loadingCtrl.create({
      message: "Cargando Lista",
      duration: 1000,
    });
    loading.present();
  }

  editProperty(propertyID) {
    this.navCtrl.navigateForward("/edit-property/" + propertyID);
  }

  deleteProperty(propertyID) {
    this.idPropiedad = propertyID;

    let body = {
      id: this.idPropiedad,
      aksi: "delproperty",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        const toast = await this.toastctrl.create({
          message: "Propiedad Eliminada",
          duration: 2000,
          cssClass: "toast-message",
        });
        toast.present();

        const loader = await this.loadingCtrl.create({
          duration: 2000,
        });

        loader.present();
        loader.onWillDismiss().then(() => {
          //lo envio a home o lo envio a login
          this.router.navigate(["/home-tenants/"]);
        });
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });

    // this.productService.deleteProduct(this.product.codigo);
    //  this.router.navigate(["./products"]);
  }

  verProperty(propertyID) {
    this.navCtrl.navigateForward("/view-property/" + propertyID);
  }
}
