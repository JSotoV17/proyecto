import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeTenantsPage } from './home-tenants.page';

describe('HomeTenantsPage', () => {
  let component: HomeTenantsPage;
  let fixture: ComponentFixture<HomeTenantsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeTenantsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeTenantsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
