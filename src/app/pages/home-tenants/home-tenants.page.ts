import { Component, OnInit } from "@angular/core";
import {
  NavController,
  MenuController,
  ToastController,
  AlertController,
  LoadingController,
  PopoverController,
  ModalController,
} from "@ionic/angular";
import { Router } from "@angular/router";

@Component({
  selector: "app-home-tenants",
  templateUrl: "./home-tenants.page.html",
  styleUrls: ["./home-tenants.page.scss"],
})
export class HomeTenantsPage implements OnInit {
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private router: Router
  ) {}

  ngOnInit() {}

  async prosesLogout() {

    this.navCtrl.navigateRoot("/");

    const toast = await this.toastCtrl.create({
      message: "Cierre de sesión Correcto.",
      duration: 2000,
      cssClass: "toast-message",
    });
    toast.present();
  }

  add() {
    this.router.navigate(["../add-property"]);
  }

  list() {
    this.router.navigate(["../list-property"]);
  }

  reservaciones(){
    this.router.navigate(["../home-results"]);
  }
}
