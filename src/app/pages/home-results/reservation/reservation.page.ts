import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Router } from "@angular/router";
import { PostProvider } from "../../../../providers/post-provider";
import { LoadingController, ToastController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-reservation",
  templateUrl: "./reservation.page.html",
  styleUrls: ["./reservation.page.scss"],
})
export class ReservationPage implements OnInit {
  formAdd: FormGroup;
  property: any = [];
  user: any = [];
  argumentos: number;
  nombre: string = "";
  id_property: number;
  user_id: string = "";
  provincia: string = "";
  direccion: string = "";
  telefono: string = "";
  ingreso: string = "";
  salida: string = "";

  constructor(
    private router: Router,
    private postPvdr: PostProvider,
    public toastctrl: ToastController,
    public loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    public activeRoute: ActivatedRoute
  ) {}

  ngOnInit() {

    this.argumentos = parseInt(this.activeRoute.snapshot.paramMap.get("id"));

    let body = {
      aksi: "listuseract",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      if (data.success) {
        this.user = data.result;
        this.user_id = this.user[0].user_id;
        this.nombre = this.user[0].username;      
        this.cargaPropiedad();
        console.log("ok");
      } else {
        console.log("no");
      }
    });
    //validations inputs

    this.formAdd = this.formBuilder.group({
      nombre: [null, Validators.compose([Validators.required])],
      provincia: [null, Validators.compose([Validators.required])],
      direccion: [null, Validators.compose([Validators.required])],
      email: [null, Validators.compose([Validators.required])],
      ingreso: [null, Validators.compose([Validators.required])],
      salida: [null, Validators.compose([Validators.required])],
    });
  }

  async cargaPropiedad() {

    let body = {
      id: this.argumentos,
      aksi: "getpropertyID",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        this.property = data.result;

        this.id_property = parseInt(this.property[0].id);
        this.provincia = this.property[0].provincia;
        this.direccion = this.property[0].direccion;
        this.telefono = this.property[0].telpropietario;

        const loading = await this.loadingCtrl.create({
          message: "Cargando Lista",
          duration: 1000,
        });
        loading.present();
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });
  }

  async addReservacion() {
    let body = {
      user_id: this.user_id,
      username: this.nombre,
      id_property: this.id_property,
      provincia: this.provincia,
      direccion: this.direccion,
      telefono: this.telefono,
      entrada: this.ingreso,
      salida: this.salida,
      aksi: "adreserv",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        const toast = await this.toastctrl.create({
          message: "Reservación Registrada",
          duration: 2000,
          cssClass: "toast-message",
        });
        toast.present();

        const loader = await this.loadingCtrl.create({
          duration: 2000,
        });

        loader.present();
        loader.onWillDismiss().then(() => {
          //lo envio a home o lo envio a login
          this.router.navigate(["/home-results"]);
        });
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });
  }
}
