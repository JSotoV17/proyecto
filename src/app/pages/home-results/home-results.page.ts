import { Component } from '@angular/core';
import { NavController, MenuController, ToastController, AlertController, LoadingController, PopoverController, ModalController } from '@ionic/angular';
import { Router } from "@angular/router";

// Modals
import { SearchFilterPage } from '../../pages/modal/search-filter/search-filter.page';
import { ImagePage } from './../modal/image/image.page';

@Component({
  selector: 'app-home-results',
  templateUrl: './home-results.page.html',
  styleUrls: ['./home-results.page.scss']
})
export class HomeResultsPage {

  searchKey = '';
  themeCover = 'assets/img/ionic4-Start-Theme-cover.jpg';
  username: String = "";


  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    private router: Router
  ) {

  }

  async prosesLogout() {

    this.navCtrl.navigateRoot('/');

    const toast = await this.toastCtrl.create({
      message: 'Cierre de sesión Correcto.',
      duration: 2000,
      cssClass: "toast-message"
    });
    toast.present();
  }

  slideOptsTop = {
    spaceBetween: 0,
    loop: true,
    autoplay: 5800,
    slidesPerView: 1.15,
  };
  /*
    slider with more narrow cards, 2 fully visible slides
  */
  slideOptsThumbs = {
    spaceBetween: 0,
    loop: true,
    autoplay: 5800,
    slidesPerView: 2.65,
  };

  list() {
    this.router.navigate(["/list"]);
  }

}