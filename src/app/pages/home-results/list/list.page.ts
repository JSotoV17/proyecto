import { Component, OnInit } from "@angular/core";
import { PostProvider } from "../../../../providers/post-provider";
import { NavController, LoadingController } from "@ionic/angular";
import { ToastController } from "@ionic/angular";
import { Router } from "@angular/router";

@Component({
  selector: 'app-list',
  templateUrl: './list.page.html',
  styleUrls: ['./list.page.scss'],
})
export class ListPage implements OnInit {
  searchTerm: string = "";
  property: any = [];
  filtro:any=[];
  idPropiedad: number;
  user_id: string = "";
  user: any = [];

  constructor( public navCtrl: NavController,
    private postPvdr: PostProvider,
    public loadingCtrl: LoadingController,
    public toastctrl: ToastController,
    private router: Router) { }

  ngOnInit() {

let body = {     
      aksi: "listproperty",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      var alertmsg = data.msg;

      if (data.success) {
        this.property = data.result;
        this.showloader();
        this.filtro = this.property;
      } else {
        const toast = await this.toastctrl.create({
          message: alertmsg,
          duration: 2000,
        });
        toast.present();
      }
    });
  }

  setFilteredItems() {
    this.filtro = this.postPvdr.filterItems(this.searchTerm, this.property);
  }

  async showloader() {
    const loading = await this.loadingCtrl.create({
      message: "Cargando Lista",
      duration: 1000,
    });
    loading.present();
  }

  verProperty(propertyID) {
    this.navCtrl.navigateForward("/view/" + propertyID);
  }

  }

