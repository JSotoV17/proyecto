import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NavController, ToastController } from "@ionic/angular";
import { PostProvider } from "../../../providers/post-provider";

@Component({
  selector: "app-loginemail",
  templateUrl: "./loginemail.page.html",
  styleUrls: ["./loginemail.page.scss"],
})
export class LoginemailPage implements OnInit {
  email: String = "";
  password: String = "";
  user_id: String = "";
  username: String = "";
  id:number =1;

  public onLoginForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    private postPvdr: PostProvider,
    private formBuilder: FormBuilder,
  ) {}

  ngOnInit() {
    this.onLoginForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])],
    });
  }

  async prosesLogin() {
    if (this.email != "" && this.password != "") {
      let body = {
        email: this.email,
        password: this.password,
        aksi: "login",
      };

      this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
        var alertpesan = data.msg;

        if (data.success) {
          this.user_id = data.result.user_id;
          this.username = data.result.username;

          if (data.result.arrendatario == 0) {
            this.navCtrl.navigateRoot("/home-results");
          } else {
            this.navCtrl.navigateRoot("/home-tenants");
          }

          const toast = await this.toastCtrl.create({
            message: "Bienvenid@ 👋 " + data.result.username,
            duration: 2000,
            cssClass: "toast-message",
          });
          toast.present();
          this.email = "";
          this.password = "";

          this.useract();
        } else {
          const toast = await this.toastCtrl.create({
            message: alertpesan,
            duration: 2000,
          });
          toast.present();
        }
      });
    } else {
      const toast = await this.toastCtrl.create({
        message: "Email o Contraseña Inválido.",
        cssClass: "toast-message",
        duration: 2000,
      });
      toast.present();
    }

    this.email = "";
    this.password = "";
  }

  async useract() {
    let body = {
      id: this.id,
      user_id: this.user_id,
      username: this.username,
      aksi: "upuseract",
    };

    this.postPvdr.postData(body, "proses-api.php").subscribe(async (data) => {
      if (data.success) {
        console.log("okLogin");
      } else {
        console.log("noLogin");
      }
    });
  }

  goToLogin() {
    this.navCtrl.navigateRoot("/");
  }
}
