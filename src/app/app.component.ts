import { Component } from '@angular/core';
import { Platform, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { Pages } from './interfaces/pages';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  username: String ="";

  public appPages: Array<Pages>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public navCtrl: NavController,
    private storage: Storage,
    public toastCtrl: ToastController
  ) {
    this.appPages = [
      {
        title: 'Inicio',
        url: '/home-results',
        direct: 'root',
        icon: 'home'
      },
      {
        title: 'Propiedades',
        url: '/list',
        direct: 'forward',
        icon: 'filing'
      },

      {
        title: 'Mis Reservaciones',
        url: '/list',
        direct: 'forward',
        icon: 'logo-buffer'
      },
      {
        title: 'Contáctenos',
        url: '/contact-us',
        direct: 'forward',
        icon: 'call'
      }
     
    ];

    this.initializeApp();

  }

  initializeApp() {

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.storage.get("user").then((res) => {      

        if (res == null) {
          this.navCtrl.navigateRoot('/');
        } else {

          this.username = res;        

        }
      })

    });

  }

  goToEditProgile() {
    this.navCtrl.navigateForward('edit-profile');
  }

  async logout() {
    this.storage.clear();

    this.navCtrl.navigateRoot('/');
    const toast = await this.toastCtrl.create({
      message: 'Cierre de sesión Correcto.',
      duration: 2000,
      cssClass: "toast-message"
    });
    toast.present();
  }
}