//providers del api

import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, } from '@angular/http';
import {Commerce} from '../../src/app/pages/modal/commerce'
import 'rxjs/add/operator/map';

@Injectable({
    providedIn: 'root'
  })
export class PostProvider {

  comercios:[];
  comercio:Commerce;

    server: string = "http://200.91.189.11/api_Progra5/" //directorio del api en ejecucion

    constructor(public http: Http) {
    }
    postData(body, file) {

        let type = "application/json; charset=UTF-8";
        let headers = new Headers({ 'Content-Type': type });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.server + file, JSON.stringify(body), options)
            .map(res => res.json());
    }

    filterItems(searchTerm, filtro) {
      return filtro.filter((item) => {
        return item.provincia.toLowerCase().indexOf(
          searchTerm.toLowerCase()) > -1;
      });
    }

    setComercios(data) {
      this.comercios = data;
    }
  
    getComercios() {
      return this.comercios;
    }

    setComercio(data) {
      this.comercio = data;
    }
  
    getComercio() {
      return this.comercio;
    }

}